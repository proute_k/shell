;; Les skeleton
(load-file "~/.emacs.d/skeleton.el")

;; Les auto
(load-file "~/.emacs.d/auto.el")

;; Les raccourcis
(load-file "~/.emacs.d/raccourcis.el")

;; Les truc utiles
(load-file "~/.emacs.d/utils.el")

;; La personnalisation
(load-file "~/.emacs.d/tunning.el")

;; Pour le respet de la norme
;;(load-file "~/.emacs.d/norme.el")

;; Autre truc
;;(load-file "~/.emacs.d/other.el")

;; Les fonctions
;;(load-file "~/.emacs.d/fonctions.el")

(put 'upcase-region 'disabled nil)

