dépot ~/bin et scripte shell
============================

ano
---
   script de norme, ``utilise norme.py``

dr
--
   gestion de dossier, permet de ce deplacer plus vite dans les dossiers mis en favorie *(by delaha_n)*

drdu
----
   script de gestin des commits, *(by delaha_n)*

mnc
---
   connection de 2 pcs via nc et transfert de fichiers automatique *(by delaha_n)*

rdu
---
   scripte de rendu

split
-----
   permet de recupérer le nom du second ecran et de le positionner correctement

   * set_screen: scripte executer au 1er demarrage d'un terminal, permet de placé le second écrand du bon coté directement
       * ``utilise le scripte split pour fonctionner``

emacs
-----
   * emacs: charge les fichier de configuration d'emacs 
       * à placer dans le **${HOME}**, modifier le nom en *.emacs*
   * emacs.d: dossier contenant tout les scriptes chargé par le .emacs 
       * à placer dans le **${HOME}**, modifier le nom en *.emacs.d*

_________________________________________________________________________________________________________________________________________________________________________
``my_clone, my_git, my_rendu, my_nv, my_select, my_wc`` made by *poirie_l*