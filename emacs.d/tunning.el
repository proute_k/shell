(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(global-linum-mode 1)
(column-number-mode t)

;;numerotation des lignes
(setq linum-format"%3d\u2502")

;;delimitation de blocs
(show-paren-mode t)

;;espace en fin de ligne
(setq show-trailing-whitespace t)
(setq-default show-trailing-whitespace t)

;;+80 colone rouge
(add-hook 'c-mode-hook '(lambda () (highlight-lines-matching-regexp".\\{81\\}" 'hi-red-b)))

;;coloration des lignes
(global-linum-mode 1)
(global-hl-line-mode 1)
	;; modification de la couleur du surlignement
(set-face-background 'hl-line "#111")
(set-face-foreground 'hl-line "#fff")

;;active la coloration syntaxique
(global-font-lock-mode t)

; Mettre un titre aux fenêtres 
(setq frame-title-format '(buffer-file-name "Emacs: %b (%f)" "Emacs: %b"))

;;change les couleurs
(cond (window-system
       (set-foreground-color "wheat")
       (set-background-color "DarkSlateGray")
       (set-cursor-color "MediumOrchid")
       (set-mouse-color "MediumOrchid")
       (set-face-foreground 'menu "wheat")
       (set-face-background 'menu "DarkSlateGray")
       (set-face-background 'fringe "DarkSlateGray")
       (set-face-foreground 'region "white")
       (set-face-background 'region "SteelBlue")
       (set-face-background 'tool-bar "DarkSlateGray")
))

