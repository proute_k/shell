;;auto-completation
(add-to-list 'load-path "/home/kevin/.emacs.d/")
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "/home/kevin/.emacs.d//ac-dict")
(ac-config-default)
(if (file-exists-p "~/.myemacs")
    (load-file "~/.myemacs"))
