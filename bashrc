#!/bin/bash

## dot.bashrc for skel in /afs/.epitech.net/users/skel
## 
## Made by rocky luke
## Login   <root@epitech.eu>
## 
## Started on  Thu Aug 28 09:58:45 2008 rocky luke
## Last update Sat Jul 12 11:47:39 2014 kévin prouteau

echo -e "\tCoucou <3"
echo -e "\t\E[01;31mT\E[01;32mr\E[01;33mo\E[01;34ml\E[01;35mo\E[01;36ml\E[01;31mo\E[01;37m"
ulimit -c 0

#xset -b

if [ "$USER" == "kevin" ]; then set_screen; fi

# System
export PATH="${PATH}:/usr/sbin:/usr/bin:/sbin:/bin"
# library
export LD_LIBRARY_PATH="/usr/local/lib"
# scripte perso
export PATH="${PATH}:${HOME}/bin"
# depot_tools dart
export PATH="${PATH}:${HOME}/Documents/prog/dart/depot_tools"

# export pour script
	# virtualenvwrapper
if [ "$USER" == "kevin" ] || [ "$USER" == "root" ]; then
    export WORKON_HOME=$HOME/.virtualenvs
    source /usr/local/bin/virtualenvwrapper.sh
fi

# environement variables

export EDITOR='emacs'
export HISTSIZE=1000
export MAIL="/u/all/${USER}/mail/${USER}"
export PAGER='less'
export SAVEHIST=1000
export WATCH='all'

# color
RED="\[\033[0;31m\]"
YELLOW="\[\033[0;33m\]"
GREEN="\[\033[0;32m\]"
WHITE="\[\033[1;37m\]"
BLUE="\[\033[0;34m\]"

LIGHT_RED="\[\033[1;31m\]"
LIGHT_GREEN="\[\033[1;32m\]"
LIGHT_GRAY="\[\033[1;37m\]"

GREEN_G="\[\033[32m\]"

RED_C="\[\033[01;31m\]"
BLUE_C="\[\033[01;34m\]"
GREEN_C="\[\033[01;32m\]"

NONE="\[\e[0m\]"

#prompt pwd crochet
if [ "$USER" == "kevin" ]; then
	SC=${GREEN_C}
	END=">"
elif [ "$USER" == "root" ]; then
	SC=${YELLOW}
	END="#"
else
	SC=${BLUE_C}
	END="$"
fi

# "user:[pwd] > ""
N_USER="${SC}${USER}${RED}:"
PROMPT="${BLUE}[${RED}\w${BLUE}]${RED} ${END}${NONE} "
export PS1="${N_USER}${PROMPT}"

# liste of my alias
	# global alias
alias j='jobs'
alias ..='cd ..'
	# alias for the ls command
alias ls='ls --color'
alias ll='ls -l'
alias l='ll'
alias la='ls -la'
alias lr='ls -R'
alias lR='ls -lR'
	# alias for the tree command
alias tree='tree -C $1'
alias t='tree $*'
alias td='tree -d $*'
alias tp='tree -p $*'
alias ta='tree -gsuph $*'
	# alias for valgrind
alias v='valgrind --track-origins=yes $*'
alias vm='valgrind --leak-check=full $*'
alias vv='valgrind --leak-check=full --show-reachable=yes -v $*'
	# alias de faignant
alias mk='acs "make -j4"'
alias mr='acs "make re"'
alias mc='acs "make clean"'
alias mfc='acs "make fclean"'
alias mrc='mr && mc'
alias mg='acs "make dbg"'
	# alias editeur
alias emacs='emacs -nw'
alias ne='emacs'
alias sne='sudo emacs -nw'
alias dne='emacs -nw --debug-init'
alias dart='~/Documents/prog/dart/dart/DartEditor'

alias b='ne /home/kevin/bin/bashrc'
# alias s='source ~/.bashrc'
alias s='source /etc/bash.bashrc'
alias sc='source $1'

alias dr='. ${HOME}/bin/dr'
alias lib='cd ~/rendu/Piscine-C-lib/my'
alias rs='/usr/bin/rm *~ 2> /dev/null; ls'
alias cs='acs'
	# game
alias mine='java -jar ~/.minecraft/Minecraft.jar'
alias ftb='java -jar ~/.ftb/launcher^FTB_Launcher.jar'

alias clean='rm -f *~;rm -f *#'

	# ssh
alias sshd='ssh root@127.0.0.1 -p 3022'

	# vagrant
alias vu='vagrant up'
alias vs='vagrant ssh'
alias vh='vagrant halt'
alias vd='vagrant destroy'
alias vst='vagrant snapshot take $1'
alias vsl='vagrant snapshot list'
alias vbs='vagrant snapshot back $1'
alias vbl='vagrant box list'

# sc()
# {
#     bash=`la ~ | grep .bashrc`
#     if [ -z $bash ]; then
# 	source $HOME/.bashrc
#     else
# 	source /etc/bash.bashrc
#     fi
# }

makc()
{
    fol=`pwd`
    tt="yoyo"
    while [ "$tt" = "yoyo" ] && [ "$(pwd)" != "/" ]
    do
	tt=`make clean 2> /dev/null || echo "yoyo"`
	if [ "$tt" = "yoyo" ]; then
	    cd ..
	fi
    done
    if [ "$tt" != "yoyo" ]; then
	echo "$tt"
    else
	echo "Nothing to clean"
    fi
    cd $fol
}

acs()
{
    flod=`pwd`
    flo=`ls | grep Makefile`
    while [ -z $flo ] && [ "$(pwd)" != "/" ]
    do
	cd ..
	flo=`ls | grep Makefile`
    done
    if [ "$(pwd)" == "/" ]; then
	echo "makefile not found"
	cd $flod
    else
	if [ ! -z "$1" ]; then
	    $1
	    cd $flod
	fi
    fi
}

myexec()
{
    while read line
    do
	plop=$(grep "NAME	=" | cut -d'	' -f3)
    done
    if [ -f $plop ]
    then
	if [ "$#" -gt "0" ] && [ "$1" == "e" ]; then
	    echo "$plop"
	else
	    echo -e "${RED}=>$plop${COLOR_NONE}"
	    ./$plop $*
	fi
    else
	make re
	make clean
	rs
	myexec $*
    fi
}

if [ -f ${HOME}/.mybashrc ]
then
    . ${HOME}/.mybashrc
fi
